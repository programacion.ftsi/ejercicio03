package org.ftsi.ejercicio03.time.profesor;

import org.ftsi.ejercicio03.time.*;
import static java.lang.System.out;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
 
/**
 * You have to find how many years and months are in 'x' minutes.
 *
 * The minutes are intruced by user from console. You must use ONLY the classes
 * that are imported on this class.
 *
 * @author	Profesor
 * @author	Andoni DÃ­az
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/time/package-summary.html">http://javadoc-javatime.oracle.com</a>
 * @see	Instant#now
 * @see ChronoUnit
 * @see	Instant#plus
 * @see	LocalDate#ofInstant
 * @see	ZoneId#systemDefault
 * @see	Period#between
 * @see	Period
 * @see System#out
 */
public class MinutsToYearsAndMonths {

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        
        Instant now = Instant.now();
        Instant later = now.plus((60*24*365*3)+(60*24)*2, ChronoUnit.MINUTES);

        LocalDate nowLocalDate = LocalDate.ofInstant(now, ZoneId.systemDefault());
        LocalDate laterLocalDate = LocalDate.ofInstant(later, ZoneId.systemDefault());

        Period between = Period.between(nowLocalDate, laterLocalDate);

        out.println("years: " + between.getYears());
        out.println("months: " + between.getMonths());
        out.println("days: " + between.getDays());
    }
}
