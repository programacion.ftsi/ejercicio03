package org.ftsi.ejercicio03.time.arkaitz;

import static java.lang.System.out;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

/**
 * 
 * Tienes que encontrar cuÃ¡ntos aÃ±os y meses hay en 'x' minutos.
 *
 * Los minutos son introducidos por el usuario desde la consola.
 * Debe utilizar SOLO las clases que se importan en esta clase
 *
 * @author	Profesor
 * @author	Andoni DÃ­az
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/time/package-summary.html">http://javadoc-javatime.oracle.com</a>
 * @see	Instant#now
 * @see ChronoUnit
 * @see	Instant#plus
 * @see	LocalDate#ofInstant
 * @see	ZoneId#systemDefault
 * @see	Period#between
 * @see	Period
 * @see System#out
 */
public class MinutsToYearsAndMonths {

    public static void main(String[] args) {
        out.println("HAZ LA PRUEBA DESPUES DE ESTA LINEA");
        
        Instant ahora = Instant.now(); 
        Instant despues = ahora.plus((60*24*365*3), ChronoUnit.MINUTES);

        LocalDate fechaAhora = LocalDate.ofInstant(ahora, ZoneId.systemDefault());
        LocalDate fechaDespues = LocalDate.ofInstant(despues, ZoneId.systemDefault());

        Period entre = Period.between(fechaAhora, fechaDespues);

        out.println("años: " + entre.getYears());
        out.println("meses: " + entre.getMonths());
        out.println("dias: " + entre.getDays());
    }
}
