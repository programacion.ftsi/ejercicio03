package org.ftsi.ejercicio03.time.joseba;

import static java.lang.System.out;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

///**
// * You have to find how many years and months are in 'x' minutes.
// *
// * The minutes are intruced by user from console. You must use ONLY the classes
// * that are imported on this class.
// *
// * @author	Profesor
// * @author	Andoni Díaz
// * @version	1.0
// * @see
// * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/time/package-summary.html">http://javadoc-javatime.oracle.com</a>
// * @see	Instant#now
// * @see ChronoUnit
// * @see	Instant#plus
// * @see	LocalDate#ofInstant
// * @see	ZoneId#systemDefault
// * @see	Period#between
// * @see	Period
// * @see System#out
// */
public class MinutsToYearsAndMonthsJoseba {

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");

        Scanner keyboard = new Scanner(System.in);

        out.println("Insert a number of minutes to convert to minutes and years:");
        long minutes = keyboard.nextLong();

        Instant currentInstant = Instant.now();
        Instant nextInstant = currentInstant.plus(minutes, ChronoUnit.MINUTES);
        
        LocalDate currentDate = LocalDate.ofInstant(currentInstant, ZoneId.systemDefault());
        LocalDate nextDate = LocalDate.ofInstant(nextInstant, ZoneId.systemDefault());
        
        Period period1 = Period.between(currentDate, nextDate);
        
        out.println("The number of minutes equals " + period1.getYears() + " years and " + period1.getMonths() + " months.");
    }
}