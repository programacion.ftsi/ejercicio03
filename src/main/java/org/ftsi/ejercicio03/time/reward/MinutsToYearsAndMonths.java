package org.ftsi.ejercicio03.time.reward;

import org.ftsi.ejercicio03.time.*;
import static java.lang.System.out;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

/**
 * You have to find how many years and months are in 'x' minutes.
 *
 * The minutes are intruced by user from console. You must use ONLY the classes
 * that are imported on this class.
 *
 * @author	Profesor
 * @author	Andoni Díaz
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/time/package-summary.html">http://javadoc-javatime.oracle.com</a>
 * @see	Instant#now
 * @see ChronoUnit
 * @see	Instant#plus
 * @see	LocalDate#ofInstant
 * @see	ZoneId#systemDefault
 * @see	Period#between
 * @see	Period
 * @see System#out
 */
public class MinutsToYearsAndMonths {
    
     //  public void now() {
           
//}
       
    public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        
       LocalDate localDate1 = LocalDate.now();
       System.out.println(localDate1);
 
       double minutosEnAno = 60 * 24 * 365;
        System.out.println("Convertir los minutos en varios años y mes.");
        System.out.println("Ingrese valor de minutos");
        double minutos = input.nextDouble();

        long anos = (long) (minutos / minutosEnAno);
        int mes = (int) (minutos / 60 / 24) % 365;
        
	

        System.out.println(minutos + " es aproximadamente " + anos + " anos."
                + "\nMinutos en los dias es: " + mes);
       
        
    }
}
