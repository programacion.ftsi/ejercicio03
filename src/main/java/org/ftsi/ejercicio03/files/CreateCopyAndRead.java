package org.ftsi.ejercicio03.files;

import static java.lang.System.out;

/**
 * You have to manipulate files on hard disk.
 *
 * Write a plain text file on your hard disk with three lines of text (any kind
 * of text). Copy the previous file with other name on the same path. Read all
 * lines of the copied file. Show each line on the console.
 *
 * @author	Profesor
 * @author	Andoni DÃ­az
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class CreateCopyAndRead {

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        
        
        
    }
}
