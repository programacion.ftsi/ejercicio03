package org.ftsi.ejercicio03.files.reward;


import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * You have to manipulate files on hard disk.
 *
 * Write a plain text file on your hard disk with three lines of text (any kind
 * of text). Copy the previous file with other name on the same path. Read all
 * lines of the copied file. Show each line on the console.
 *
 * @author	Profesor
 * @author	Andoni Díaz
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class RewardFile {
    
    public void Reward(){
        try {
            Path path = null;
            try {
                path = Files.createTempFile("test-file", ".txt");
            } catch (IOException ex) {
                Logger.getLogger(RewardFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            Iterable<String> iterable = Arrays.asList("line1", "line2");
            Path write = Files.write(path, iterable);
            
            List<String> lines = Files.readAllLines(path);
            for (String line:lines){
                
            }
            byte[] bytes = Files.readAllBytes(path);
            System.out.println(new String(bytes));
            
        } catch (IOException ex) {
            Logger.getLogger(RewardFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        RewardFile file = new RewardFile();
        file.Reward();
//        String fileSeparator = System.getProperty("file.separator");
//        
//        try {
//            Files.write(Paths.get("test.txt"), "test\ntest\ntest\n".getBytes());
//            Files.copy(Path,Path);
//            Files.copy(Paths.get("test.txt"), Paths.get("test2.txt"));
//            
//            Files.readAllBytes(path, path);
//            
//        } catch (IOException ex) {
//            Logger.getLogger(RewardFile.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
      //  Path path = Files.createTempFile("test-file", ".txt");
      //  Iterable<String> iterable = Arrays.asList("line1", "line2");
       // Files.write(path, iterable);

       // byte[] bytes = Files.readAllBytes(path);
        //System.out.println(new String(bytes));

    }
}