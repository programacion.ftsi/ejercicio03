package org.ftsi.ejercicio03.files.adrian;



import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Paths;



import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tienes que manipular los archivos en el disco duro.
 *
 * 
 * Escriba un archivo de texto plano en su disco duro con tres líneas de texto 
 * (cualquier tipo de texto). Copie el archivo anterior con otro nombre en la misma ruta.
 * Lea todas las líneas del archivo copiado. Mostrar cada línea en la consola.
 *
 * @author	Profesor
 * @author	Andoni Díaz
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class CreateCopyAndRead {

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        try {
            Files.write(Paths.get("C:\\Programacion\\Ejficheros\\file.txt"), "line1\nline2\nline3\n".getBytes());
            Files.copy(Paths.get("C:\\Programacion\\Ejficheros\\file.txt"), Paths.get("C:\\Programacion\\Ejficheros\\file2.txt"));
            Files.lines(Paths.get("C:\\Programacion\\Ejficheros\\file.txt")).forEach(System.out::println);

        } catch (IOException ex) {







            Logger.getLogger(CreateCopyAndRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
