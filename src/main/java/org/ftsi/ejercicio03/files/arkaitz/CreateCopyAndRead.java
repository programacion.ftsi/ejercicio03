package org.ftsi.ejercicio03.files.arkaitz;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tienes que manipular los archivos en el disco duro.
 *
 * 
 * Escriba un archivo de texto plano en su disco duro con tres líneas de texto 
 * (cualquier tipo de texto). Copie el archivo anterior con otro nombre en la misma ruta.
 * Lea todas las líneas del archivo copiado. Mostrar cada línea en la consola.
 *
 * @author	Profesor
 * @author	Andoni Díaz
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class CreateCopyAndRead {

    public static void main(String[] args) throws IOException {
        out.println("HAZ LA PRUEBA DESPUÉS DE ESTA LÍNEA, EN ESTE MÉTODO.");
        try{
            //Crear el archivo de texto
            Files.write(Paths.get("prueba.txt"), "linea1 \nlinea2 \nlinea3 \n".getBytes());
           //Hacemos una copia
            Files.copy(Paths.get("prueba.txt"),Paths.get("prueba2.txt"));
            
            List<String> readAllLines = Files.readAllLines(Paths.get("prueba2.txt"));
           
            for (String linea : readAllLines) {
             System.out.println(linea);
            }
            
        }catch(IOException ex) {
            Logger.getLogger(CreateCopyAndRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}