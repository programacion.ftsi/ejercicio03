/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ftsi.ejercicio03.files.kevin;

import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kevinFTSI
 */
public class CreateCopyAndRead {

    public static void main(String[] args) throws IOException {
        out.println("HAZ LA PRUEBA DESPUÉS DE ESTA LÍNEA, EN ESTE MÉTODO.");
        try{
            Files.write(Paths.get("prueba.txt"), "linea1 \nlinea2 \nlinea3 \n".getBytes());
            
            Files.copy(Paths.get("prueba.txt"),Paths.get("prueba2.txt"));
            
            List<String> readAllLines = Files.readAllLines(Paths.get("prueba2.txt"));
           
            for (String linea : readAllLines) {
             System.out.println(linea);
            }
            
        }catch(IOException ex) {
            Logger.getLogger(CreateCopyAndRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}