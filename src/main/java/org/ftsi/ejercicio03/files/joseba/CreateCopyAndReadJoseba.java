package org.ftsi.ejercicio03.files.joseba;

import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import java.nio.file.attribute.FileAttribute;

/**
 * You have to manipulate files on hard disk.
 *
 * Write a plain text file on your hard disk with three lines of text (any kind
 * of text). Copy the previous file with other name on the same path. Read all
 * lines of the copied file. Show each line on the console.
 *
 * @author	Profesor
 * @author	Andoni Díaz
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class CreateCopyAndReadJoseba {
    
    public static void main(String[] args) throws IOException {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        
        Files.write(Paths.get("./text.txt"), "line1\nline2\nline3\n".getBytes());
        
        //Files.copy(Paths.get("./text.txt"), Paths.get("./text2.txt"));
        
        Files.lines(Paths.get("./text.txt")).forEach(System.out::println);
        
        
        
    }
}