package org.ftsi.ejercicio03.files.jesus;

import org.ftsi.ejercicio03.files.profesor.*;
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * You have to manipulate files on hard disk.
 *
 * Write a plain text file on your hard disk with three lines of text (any kind
 * of text). Copy the previous file with other name on the same path. Read all
 * lines of the copied file. Show each line on the console.
 *
 * @author	Profesor
 * @author	Andoni DÃ­az
 * @version	1.0
 * @see
 * <a href="https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/nio/file/package-summary.html">http://javadoc-javafilenio.oracle.com</a>
 */
public class CreateCopyAndRead {

    public static void main(String[] args) {
        out.println("DO THE TEST AFTER THIS LINE, ON THIS METHOD.");
        try {
            Files.write(Paths.get("test.txt"), "line1\nline2\nline3\n".getBytes());
            Files.copy(Paths.get("test.txt"), Paths.get("test2.txt"));
            List<String> lines = Files.readAllLines(Paths.get("test2.txt"));
            for (String line : lines) {
                out.println(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(CreateCopyAndRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
